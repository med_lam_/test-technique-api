<?php
namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class OccuranceTest extends WebTestCase
{
    public function testLargestOccurance()
    {
        
        $client = static::createClient();
        $client->request('GET', '/occurance/aaabcbbbccaaaaaaaabbdd');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_encode($response->getContent(), true);
        $this->assertJsonStringEqualsJsonString($responseData, json_encode('[{"input":"aaabcbbbccaaaaaaaabbdd"},{"result":"a"}]'));
        
    }

    public function testlistPaginationAttemptsOfLargestOccurance()
    {
        $client = static::createClient();
        $client->request('GET', '/list-pagination-attempts/2');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_encode($response->getContent(), true);
        $this->assertJsonStringEqualsJsonString($responseData, json_encode('["number of elements : 8",[[{"input":"aaabcbbbcccccccccccccaaabbdd"},{"result":"c"}],[{"input":"aaabcbbbbbbbbbbbbbbbbbbccaaaaaaaabbdd"},{"result":"b"}]],[[{"input":"aaabcbbbbbbbbbccaaaaaaaabbdd"},{"result":"b"}],[{"input":"aaabcbbbccaaaaaaaabbdd"},{"result":"a"}]],[[{"input":"aaabcbbbccaaaaaaaabbbbdd"},{"result":"a"}],[{"input":"aaabcbbbccaaaaaaaaaabbdd"},{"result":"a"}]],[[{"input":"aaabcbbbccaaaaaaaaaaabbdd"},{"result":"a"}],[{"input":"aaabcbbccccccbccaaabbdd"},{"result":"c"}]]]'));
    }

}
