<?php

namespace App\Service;


Class OccuranceService 
{
    public function largestOccurance($str) : array
    {
        $len = strlen($str);
        $count = 0;
          
        // Find the maximum repeating character
        // starting from str[i]
        $result = $str[0];
        for ($i = 0; $i < $len; $i++)
        {
             $currentCount = 1;
            for ($j = $i+1; $j < $len; $j++)
            {
                if ($str[$i] != $str[$j])
                    break;
                $currentCount++;
            }
     
            // Update result if required
            if ($currentCount > $count)
            {
                $count = $currentCount;
                $result = $str[$i];
            }
        }

        return [['input' => $str], ['result' => $result]];

    }

}


