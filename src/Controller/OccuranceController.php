<?php
namespace App\Controller;


use App\Service\OccuranceService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class OccuranceController extends AbstractController
{

    #[Route('/occurance/{str}', name:'largest_occurance', methods: ['GET'])]
    public function largestOccurance(String $str, OccuranceService $occuranceService) : JsonResponse 
    {
        return $this->json($occuranceService->largestOccurance($str));
    }

    #[Route('/list-pagination-attempts/{pagination}', name:'list-pagination-attempts', methods: ['GET'])]
    public function listPaginationAttemptsOfLargestOccurance(int $pagination, OccuranceService $occuranceService) : JsonResponse
    {
        $input = array('aaabcbbbcccccccccccccaaabbdd', 'aaabcbbbbbbbbbbbbbbbbbbccaaaaaaaabbdd', 'aaabcbbbbbbbbbccaaaaaaaabbdd', 'aaabcbbbccaaaaaaaabbdd',
        'aaabcbbbccaaaaaaaabbbbdd', 'aaabcbbbccaaaaaaaaaabbdd', 'aaabcbbbccaaaaaaaaaaabbdd', 'aaabcbbccccccbccaaabbdd'); 

        foreach ($input as $value){
               $result[] =  $occuranceService->largestOccurance($value);
        }
        $result = array_chunk($result, $pagination);
        array_unshift($result , 'number of elements : '.count($input));

        return $this->json($result);
    }

}
